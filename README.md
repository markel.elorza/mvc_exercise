#MVC-Exercise 1 (User Management)
* Create a new project based on 11 DAO User.
* Add CRUD functionalities (Create Read Update Delete):
Create User. (Create)
View User List. (Read)
Single User information. (Read)
Edit User. (Update)
Delete User. (Delete)
* UserController Actions:
GET /user & GET /user?action=list -> Displatch user_list.jsp
GET /user?action=view&userId={id} -> Dispatch user.jsp with user data.
GET /user?action=create -> Dispatch user_form.jsp (empty)
POST /user?action=create -> Store user in DB and redirect to /user?action=view&userId={id}
GET /user?action=edit&userId={id} -> Dispatch user_form.jsp with the user filled
POST /user?action=edit&userId={id} -> Update user in DB and redirect to /user?action=view&userId={id}
GET /user?action=delete&userId={id}-> Delete user from DB and redirect to /user?action=list
* Create/modify any needed file.
Functions in DAO (Model)
JSP (View)
Servlets (Controller)
* index.jsp should be the only JSP file that appears in the URL. (Use dispatch and redirect to Controllers instead of JSPs)
* Make it Muti-lingual.

